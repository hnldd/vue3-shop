module.exports = {
    publicPath: '/dist', // 构建好的文件输出到哪里
    devServer: {
        proxy: {
            '/admin': {
                // 此处的写法，目的是为了 将 /api 替换成 https://www.baidu.com/
                target: 'http://apis.lxkj828.com/admin/',
                // 允许跨域
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    '^/admin': ''
                }
            }
        }
    }
}
