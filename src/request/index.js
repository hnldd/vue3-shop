import requestTool from './axios-tools'
const requestApis = {
    login: function (data) {
        return requestTool.post('login/login', data)
    },
    getRouterMenu: function (data) {
        return requestTool.post('login/getRouterMenu', data)
    },
    getLeftMenu: function (data) {
        return requestTool.post('login/getLeftMenu', data)
    },
    getAdminInfo: function (data) {
        return requestTool.post('admin/getAdminInfo', data)
    },
    saveAdminInfo: function (data) {
        return requestTool.post('admin/saveAdminInfo', data)
    },
    getTagConfig: function (data) {
        return requestTool.post('config/getTagConfig', data)
    },
    /* 管理员相关接口 */
    getAdminListPage: function (data) {
        return requestTool.post('admin/getAdminListPage', data)
    },
    saveAdminAdd: function (data) {
        return requestTool.post('admin/saveAdminAdd', data)
    },
    getAdminRow: function (data) {
        return requestTool.post('admin/getAdminRow', data)
    },
    saveAdminEdit: function (data) {
        return requestTool.post('admin/saveAdminEdit', data)
    },
    saveAdminDel: function (data) {
        return requestTool.post('admin/saveAdminDel', data)
    },
    setAdminPassword: function (data) {
        return requestTool.post('admin/setAdminPassword', data)
    },
    saveAdminPower: function (data) {
        return requestTool.post('admin/saveAdminPower', data)
    },
    getAdminPower: function (data) {
        return requestTool.post('admin/getAdminPower', data)
    },
    getAdminApis: function (data) {
        return requestTool.post('admin/getAdminApis', data)// 获取管理员拥有权限
    },
    /* 菜单管理接口 */
    getMenuTree: function (data) {
        return requestTool.post('menu/getMenuTree', data)
    },
    saveMenuAdd: function (data) {
        return requestTool.post('menu/saveMenuAdd', data)
    },
    getMenuRow: function (data) {
        return requestTool.post('menu/getMenuRow', data)
    },
    saveMenuEdit: function (data) {
        return requestTool.post('menu/saveMenuEdit', data)
    },
    saveMenuDel: function (data) {
        return requestTool.post('menu/saveMenuDel', data)
    },
    getMenuPower: function (data) {
        return requestTool.post('menu/getMenuPower', data)
    },
    saveMenuPower: function (data) {
        return requestTool.post('menu/saveMenuPower', data)
    },
    getTreeMenuAll: function (data) {
        return requestTool.post('menu/getTreeMenuAll', data)
    },
    getApisList: function (data) {
        return requestTool.post('power/getApisList', data)
    },
    getGroupList: function (data) {
        return requestTool.post('group/getGroupList', data)
    },
    saveGroupAdd: function (data) {
        return requestTool.post('group/saveGroupAdd', data)
    },
    getGroupRow: function (data) {
        return requestTool.post('group/getGroupRow', data)
    },
    saveGroupEdit: function (data) {
        return requestTool.post('group/saveGroupEdit', data)
    },
    saveGroupDel: function (data) {
        return requestTool.post('group/saveGroupDel', data)
    },
    getGroupPower: function (data) {
        return requestTool.post('group/getGroupPower', data)
    },
    saveGroupPower: function (data) {
        return requestTool.post('group/saveGroupPower', data)
    },
    getTreeGroupAll: function (data) {
        return requestTool.post('group/getTreeGroupAll', data)// 设置上下级关系时使用
    },
    getGroupPowerAll: function (data) {
        return requestTool.post('group/getGroupPowerAll', data) // 获取权限组带权限树在给角色授权时使用
    },
    getRolesList: function (data) {
        return requestTool.post('roles/getRolesList', data)
    },
    saveRolesAdd: function (data) {
        return requestTool.post('roles/saveRolesAdd', data)
    },
    getRolesRow: function (data) {
        return requestTool.post('roles/getRolesRow', data)
    },
    saveRolesEdit: function (data) {
        return requestTool.post('roles/saveRolesEdit', data)
    },
    saveRolesDel: function (data) {
        return requestTool.post('roles/saveRolesDel', data)
    },
    getRolesPower: function (data) {
        return requestTool.post('roles/getRolesPower', data)
    },
    saveRolesPower: function (data) {
        return requestTool.post('roles/saveRolesPower', data)
    },
    getTreeRolesAll: function (data) {
        return requestTool.post('roles/getTreeRolesAll', data)
    },
    getRoleAll: function (data) {
        return requestTool.post('roles/getRoleAll', data)
    },
    getMenuIds: function (data) {
        return requestTool.post('roles/getMenuIds', data)
    },
    getConfigListPage: function (data) {
        return requestTool.post('config/getConfigListPage', data)
    },
    saveConfigAdd: function (data) {
        return requestTool.post('config/saveConfigAdd', data)
    },
    getConfigRow: function (data) {
        return requestTool.post('config/getConfigRow', data)
    },
    saveConfigEdit: function (data) {
        return requestTool.post('config/saveConfigEdit', data)
    },
    saveConfigDel: function (data) {
        return requestTool.post('config/saveConfigDel', data)
    },
    getCategoryConfig: function (data) {
        return requestTool.post('config/getCategoryConfig', data)
    },
    saveCategoryConfig: function (data) {
        return requestTool.post('config/saveCategoryConfig', data)
    },
    setResourceStatus: function (data) {
        return requestTool.post('resource/setResourceStatus', data)
    },
    getNoticeListPage: function (data) {
        return requestTool.post('notice/getNoticeListPage', data)
    },
    saveNoticeAdd: function (data) {
        return requestTool.post('notice/saveNoticeAdd', data)
    },
    getNoticeRow: function (data) {
        return requestTool.post('notice/getNoticeRow', data)
    },
    saveNoticeEdit: function (data) {
        return requestTool.post('notice/saveNoticeEdit', data)
    },
    saveNoticeDel: function (data) {
        return requestTool.post('notice/saveNoticeDel', data)
    },
    getBannerListPage: function (data) {
        return requestTool.post('banner/getBannerListPage', data)
    },
    saveBannerAdd: function (data) {
        return requestTool.post('banner/saveBannerAdd', data)
    },
    getBannerRow: function (data) {
        return requestTool.post('banner/getBannerRow', data)
    },
    saveBannerEdit: function (data) {
        return requestTool.post('banner/saveBannerEdit', data)
    },
    saveBannerDel: function (data) {
        return requestTool.post('banner/saveBannerDel', data)
    },
    getColumnTreeAll: function (data) {
        return requestTool.post('column/getColumnTreeAll', data)
    },
    saveColumnAdd: function (data) {
        return requestTool.post('column/saveColumnAdd', data)
    },
    getColumnRow: function (data) {
        return requestTool.post('column/getColumnRow', data)
    },
    saveColumnEdit: function (data) {
        return requestTool.post('column/saveColumnEdit', data)
    },
    saveColumnDel: function (data) {
        return requestTool.post('column/saveColumnDel', data)
    },
    getColumnTree: function (data) {
        return requestTool.post('column/getColumnTree', data)
    },
    getArticleCategoryTreeAll: function (data) {
        return requestTool.post('article_category/getArticleCategoryTreeAll', data)
    },
    saveArticleCategoryAdd: function (data) {
        return requestTool.post('article_category/saveArticleCategoryAdd', data)
    },
    getArticleCategoryRow: function (data) {
        return requestTool.post('article_category/getArticleCategoryRow', data)
    },
    saveArticleCategoryEdit: function (data) {
        return requestTool.post('article_category/saveArticleCategoryEdit', data)
    },
    saveArticleCategoryDel: function (data) {
        return requestTool.post('article_category/saveArticleCategoryDel', data)
    },
    getArticleCategoryTree: function (data) {
        return requestTool.post('article_category/getArticleCategoryTree', data)
    },
    getArticleListPage: function (data) {
        return requestTool.post('article/getArticleListPage', data)
    },
    saveArticleAdd: function (data) {
        return requestTool.post('article/saveArticleAdd', data)
    },
    getArticleRow: function (data) {
        return requestTool.post('article/getArticleRow', data)
    },
    saveArticleEdit: function (data) {
        return requestTool.post('article/saveArticleEdit', data)
    },
    saveArticleDel: function (data) {
        return requestTool.post('article/saveArticleDel', data)
    },
    uploadImg: function (data) {
        return requestTool.post('resource/uploadImg?group=tinymce', data)
    },
    getLinksListPage: function (data) {
        return requestTool.post('links/getLinksListPage', data)
    },
    saveLinksAdd: function (data) {
        return requestTool.post('links/saveLinksAdd', data)
    },
    getLinksRow: function (data) {
        return requestTool.post('links/getLinksRow', data)
    },
    saveLinksEdit: function (data) {
        return requestTool.post('links/saveLinksEdit', data)
    },
    saveLinksDel: function (data) {
        return requestTool.post('links/saveLinksDel', data)
    },
    getPhotoListPage: function (data) {
        return requestTool.post('photo/getPhotoListPage', data)
    },
    getPhotoInfo: function (data) {
        return requestTool.post('photo/getPhotoInfo', data)
    },
    getResourceGaroupList: function (data) {
        return requestTool.post('resource/getResourceGaroupList', data)
    },
    saveResourceGaroupAdd: function (data) {
        return requestTool.post('resource/saveResourceGaroupAdd', data)
    },
    getResourceGaroupRow: function (data) {
        return requestTool.post('resource/getResourceGaroupRow', data)
    },
    saveResourceGaroupEdit: function (data) {
        return requestTool.post('resource/saveResourceGaroupEdit', data)
    },
    saveResourceGaroupDel: function (data) {
        return requestTool.post('resource/saveResourceGaroupDel', data)
    },
    getResourceGaroupAll: function () {
        return requestTool.post('resource/getResourceGaroupAll')
    }
}
export default requestApis
