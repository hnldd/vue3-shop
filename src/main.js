import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import VueParticles from 'vue-particles'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import '@/assets/css/global.css'
const asynRouter = JSON.parse(sessionStorage.getItem('asynRouter'))
if (asynRouter !== null) {
    asynRouter.forEach(item => {
        router.addRoute('Home', {
            path: '/' + item.component_path,
            name: item.name,
            component: () => import(/* webpackChunkName: "about" */ '@/views/' + item.component_path)
        })
    })
}
createApp(App).use(store).use(router).use(ElementPlus).use(VueParticles).mount('#app')
