import { createStore } from 'vuex'

export default createStore({
    state: {
        token: '',
        asynRouter: []
    },
    mutations: {
        INSERT_TOKEN: function (state, token) {
            state.token = token
        },
        SET_ROUTER: function (state, router) {
            state.asynRouter = router
        },
        GET_ROUTER: function (state) {
            return state.asynRouter
        }
    },
    actions: {
        setToken (context, token) {
            context.commit('INSERT_TOKEN', token)
        },
        setRouter (context, router) {
            context.commit('SET_ROUTER', router)
        },
        getRouter (context) {
            context.commit('GET_ROUTER')
        }
    },
    modules: {
    }
})
